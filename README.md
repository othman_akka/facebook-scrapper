welcome to facebook scrapper tasks you can copy the project by :

```bash
    git clone https://gitlab.com/brain_storm/facebook_scrapper.git
```

then create your own virtual environment by:

```bash
    python3 -m venv venv
```

then install the requirement by

```bash
    pip install -r requirements.txt
```
if you updated the requirement please update the requirements.txt by:

```bash
    pip freeze > requirements.txt
```

tasks:

- [x] spider to login and confirm the login
- [ ] spider to get friends of user
- [ ] spider to make invitation to a groupe
- [ ] spider to make invitation to a person
- [ ] spider to make posts from given text
- [ ] spider to like a given post url
- [ ] spider to make comment to given post url