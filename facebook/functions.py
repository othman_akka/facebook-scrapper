import base64

class Functions():

    def save_png(data, name):
        imgdata = base64.b64decode(data)
        with open(name, 'wb') as f:
            f.write(imgdata)
            f.close()



