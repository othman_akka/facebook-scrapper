# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst


class FacebookItem(scrapy.Item):
    images_urls = scrapy.Field()
    images = scrapy.Field()
    profile_name = scrapy.Field(output_processor = TakeFirst())



