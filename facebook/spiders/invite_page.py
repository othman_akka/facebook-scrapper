from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import time

class Invite_page(Base):
    name = 'invite_page'
    redis_key = 'invite_page:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        page_url = response.meta['profile_url']
        text = response.meta['text']
        driver = self.drivers[driver_index]
        #driver.maximize_window()

        # go to page
        driver.get(page_url)

        driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(5)

        try:
            page = "//div[@class='_5hgz _51xa']/div/div/span/button"
            page = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, page)))
            print("Page is ready!")
            page.click()
        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'profile_url': page_url}
        driver.close()
        self.drivers[driver_index] = None