from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import time

class Followers_profile(Base):
    name = 'followers_profile'
    redis_key = 'followers_profile:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        profile_url = response.meta['profile_url']
        text = response.meta['text']
        driver = self.drivers[driver_index]
        #driver.maximize_window()

        # go to profile
        driver.get(profile_url)
        followers = []

        driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(5)
        try:
            profile = "//span[@class='_50f8 _2iem']"
            all_profile = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, profile)))
            print("Page is ready!")
            if all_profile.text != "":
                link_friends = all_profile.find_element_by_xpath(".//a[1]")
                driver.get(link_friends.get_attribute("href"))
                # for uploading a web page some complete
                for i in range(1, 20):
                    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
                    time.sleep(3)

                div_ul = "//div[@class='_5h60 _30f']"
                all_ul = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, div_ul)))

                if all_ul != None :
                    for ul in all_ul.find_elements_by_xpath(".//ul"):
                        if ul != None:
                            for li in ul.find_elements_by_xpath(".//li[@class='_698']"):
                                if li != None :
                                    link = li.find_element_by_xpath(".//div[@class ='fsl fwb fcb']/a")
                                    if link != None:
                                        followers.append(str(link.get_attribute('href')))
                                        print("hh ",str(link.get_attribute('href')))
                                    else:
                                        print("link is null!")
                                else:
                                    print("li is null")
                        else:
                            print("ul is null")
                else:
                    print("all_ulid Null!")
            else:
                print("Not found friends!")
            driver.save_screenshot("image.png")
        except TimeoutException:
            print("this Profile without followers!")
            return [-1, followers]
        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'profile_url': profile_url, 'followers': followers}
        driver.close()
        self.drivers[driver_index] = None

        # 1 for this profile have followers
        return [1, followers]
