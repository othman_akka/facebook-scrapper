from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
import time

class Invite_persons(Base):
    name = 'invite_person'
    redis_key = 'invite_person:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        profile_url = response.meta['profile_url']
        text = response.meta['text']
        driver = self.drivers[driver_index]
        #driver.maximize_window()

        # go to profile of person
        driver.get(profile_url)

        driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(5)
        try:
            profile = "//div[@class='_2yfv _2yfv FriendButton']"
            button = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, profile)))
            print("Page is ready!")
            if button != None:
                driver.implicitly_wait(10)
                ActionChains(driver).move_to_element(button).click(button).perform()

                for i in range(1, 3):
                    driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
                    time.sleep(5)

                div_hide = "//div[@class='_10 uiLayer _4-hy _3qw']"
                div_hide = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, div_hide)))

                if div_hide != None:
                    text = str(div_hide.text)
                    if text.split("\n")[3] == "Close":
                        print("close close")
                        div_close = ".//div[@class='_5a8u _5lnf uiOverlayFooter']"
                        div_close = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, div_close)))
                        if div_close:
                            button = div_close.find_element_by_xpath(".//div[@class='clearfix']/div[2]")
                            driver.implicitly_wait(10)
                            ActionChains(driver).move_to_element(button).click(button).perform()
                            print("this compte isn't available")
                    else:
                        div_confirmer = ".//div[@class='_5lnf uiOverlayFooter _5a8u']"
                        div_confirmer = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, div_confirmer)))
                        if div_confirmer:
                            button = div_confirmer.find_element_by_xpath(".//button[@class='_42ft _4jy0 layerConfirm uiOverlayButton _4jy3 _4jy1 selected _51sy']")
                            driver.implicitly_wait(10)
                            ActionChains(driver).move_to_element(button).click(button).perform()
                            print("confirm!")
                        else:
                            print("cancel")
                else:
                    print("send succeful")
            else:
                print("Button is null")
            driver.save_screenshot("image.png")
        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'profile_url': profile_url}
        driver.close()
        self.drivers[driver_index] = None