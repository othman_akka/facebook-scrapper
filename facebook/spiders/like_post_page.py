from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
import time

class Like_post_page(Base):
    name = 'like_post_page'
    redis_key = 'like_post_page:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        page_url = response.meta['profile_url']
        post_id = response.meta['post_id']
        text = response.meta['text']
        driver = self.drivers[driver_index]
        #driver.maximize_window()

        # go to page
        driver.get(page_url)
        for i in range(1, 10):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(5)
        try:
            posts = "//div[@id='pagelet_timeline_main_column']"
            all_posts = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, posts)))
            all_posts.find_element_by_xpath(".//div[@class='_1xnd']")

            if all_posts.text != "":
                for post_field in all_posts.find_elements_by_xpath(".//div[@class='_4-u2 _4-u8']"):
                    post = post_field.find_element_by_xpath(".//div[@class='_6a _5u5j _6b']/div[1]")
                    if str(post.get_attribute("id")) ==post_id:
                        like_pane = post_field.find_element_by_xpath(".//div[@class='_78bu']")
                        like_pane = like_pane.find_element_by_xpath(".//div[@class='_3vuz']/span[1]")
                        driver.implicitly_wait(10)
                        ActionChains(driver).move_to_element(like_pane).click(like_pane).perform()
                        print("like succeful!")
                        break
            else:
                print("problem in posts!")
        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'page_url': page_url}
        driver.close()
        self.drivers[driver_index] = None

