from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import time

class followers_groupe(Base):
    name = 'followers_group'
    redis_key = 'followers_group:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        group_url = response.meta['profile_url']
        text = response.meta['text']
        driver = self.drivers[driver_index]

        # go to groupe
        driver.get(group_url)

        followers = []
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(5)
        try:
            members = "//div[@title='Members']"
            all_members = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, members)))
            print("Page is ready!")
            all_members.click()

            for i in range(1,40):
                driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
                time.sleep(5)

            div_all = "//div[@id='groupsMemberBrowserContent']"
            div_all = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, div_all)))

            if div_all != None:
                print("members are ready!")
                div_all = div_all.find_element_by_xpath(".//div[@id='groupsMemberSection_recently_joined']")
                div_list1 = div_all.find_element_by_xpath(".//div[@class='fbProfileBrowserList fbProfileBrowserListContainer']/ul")
                if div_list1 != None:
                    for div in div_list1.find_elements_by_xpath(".//div[@class='clearfix _60rh _gse']"):
                        link = div.find_element_by_xpath(".//a")
                        #followers.append(str(link.get_attribute('href')).split("?")[0])
                        followers.append(str(link.get_attribute('href')))
                else:
                    print("div_list1 is Null")

                div_list = div_all.find_element_by_xpath(".//div[@class='fbProfileBrowserList fbProfileBrowserListContainer']")
                if div_list != None:
                    for div in div_list.find_elements_by_xpath(".//div[@class='fbProfileBrowserList expandedList']"):
                        ul = div.find_element_by_xpath(".//ul")
                        if ul != None:
                            for div in ul.find_elements_by_xpath(".//div[@class='clearfix _60rh _gse']"):
                                link = div.find_element_by_xpath(".//a")
                                followers.append(str(link.get_attribute('href')))
                        else:
                            print("ul is Null")
                else:
                    print("div_list is Null")
            else:
                print("members aren't ready!")

            driver.save_screenshot("image.png")
        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'group_url': group_url, 'followers': followers}
        driver.close()
        self.drivers[driver_index] = None
        return result