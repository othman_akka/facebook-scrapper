from bs4 import BeautifulSoup
from facebook.spiders.base import Base

class Test(Base):
    name = 'test_login_facebook'
    redis_key = 'test_login:facebook'

    def parse(self, response):

        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        profile_url = response.meta['profile_url']
        text = response.meta['text']
        driver = self.drivers[driver_index]
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')

        # logic if login is success or not test with facebook name
        login = False
        try:
            profile_name = soup.find('div', attrs={'dir': 'ltr'}).get_text()
            login = True
        except:
            pass
        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        if login:
            result = {'user': user, 'pass': pw, 'ip': ip, 'login': login, 'profile_name': profile_name}
        else:
            result = {'user': user, 'pass': pw, 'ip': ip, 'login': login}
        driver.close()
        self.drivers[driver_index] = None
        yield result
        print(soup)

