from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
import time

class followers_page(Base):
    name = 'followers_page'
    redis_key = 'followers_page:facebook'

    def parse(self, response):
        # i don't
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        page_url = response.meta['profile_url']
        post_id = response.meta['post_id']
        text = response.meta['text']
        driver = self.drivers[driver_index]

        # go to page
        driver.get(page_url)

        followers = []
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(5)
        try:
            posts = "//body[@class='_4-u5 _2yq _-kb _605a q_wk1uwrgbk chrome webkit win x1-5 Locale_en_GB cores-gte4 hasAXNavMenubar _19_u']"
            all_posts2 = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, posts)))
            all_posts = all_posts2.find_element_by_xpath(".//div[@class='_1xnd']")

            if all_posts.text != "":
                for post_field in all_posts.find_elements_by_xpath(".//div[@class='_4-u2 _4-u8']"):
                    post = post_field.find_element_by_xpath(".//div[@class='_6a _5u5j _6b']/div[1]")

                    if str(post.get_attribute("id")) == post_id:
                        # comment
                        all_comments = post_field.find_element_by_xpath(".//div[@class='_3w53']")
                        get_all_li = all_comments.find_element_by_xpath(".//div[@class='_3tz_ _7794']")

                        if get_all_li.text != "":
                            view_more_comments = get_all_li.find_element_by_xpath(".//div[@class='_4swz _293g']")

                            driver.implicitly_wait(5)
                            ActionChains(driver).move_to_element(view_more_comments).click(view_more_comments).perform()

                            driver.execute_script("return arguments[0].scrollIntoView(true);", all_comments)
                            time.sleep(2)

                        ul = all_comments.find_element_by_xpath(".//ul[@class='_7791']")
                        for li_field in ul.find_elements_by_xpath("li"):
                            link = li_field.find_element_by_xpath(".//a[@class='_6qw4']")
                            followers.append(str(link.get_attribute('href')))

                            # if existe a lot of reply
                            reply_all = li_field.find_element_by_xpath(".//div[2]")
                            if reply_all.text != "":
                                driver.implicitly_wait(5)
                                ActionChains(driver).move_to_element(reply_all).click(reply_all).perform()

                                driver.execute_script("return arguments[0].scrollIntoView(true);", reply_all)
                                time.sleep(2)

                                ul = li_field.find_element_by_xpath(".//ul")
                                for li in ul.find_elements_by_xpath("li"):
                                    link = li_field.find_element_by_xpath(".//a[@class='_6qw4']")
                                    followers.append(str(link.get_attribute('href')))

                        # likes
                        likes = post_field.find_element_by_xpath(".//div[@class='_4299']")
                        span_likes = likes.find_element_by_xpath(".//span[@class='_3dlh _3dli']")
                        driver.implicitly_wait(10)
                        ActionChains(driver).move_to_element(span_likes).click(span_likes).perform()
                        time.sleep(4)
                        driver.execute_script("return arguments[0].scrollIntoView(true);", all_posts2)
                        time.sleep(5)
                        likes = all_posts2.find_element_by_xpath(".//div[@class='_10 uiLayer _4-hy _3qw']")
                        likes = likes.find_element_by_xpath(".//ul[@class='uiList _4kg']")
                        for profile in likes.find_elements_by_xpath(".//li"):
                            link = profile.find_element_by_xpath(".//a[@class='_5i_s _8o _8r lfloat _ohe']")
                            followers.append(str(link.get_attribute('href')))
                        print("get succeful!")
                        break
            else:
                print("problem in posts!")
        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        followers = set(followers)
        result = {'user': user, 'pass': pw, 'ip': ip, 'profile_url': page_url, 'followers': followers}
        driver.close()
        self.drivers[driver_index] = None