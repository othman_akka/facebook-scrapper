from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
import time

class Text_post_page(Base):
    name = 'post_text_page'
    redis_key = 'post_text_page:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        page_url = response.meta['profile_url']
        text = response.meta['text']
        driver = self.drivers[driver_index]

        # go to page
        driver.get(page_url)
        for i in range(1, 2):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(5)

        try:
            div = "//div[@role='region']"
            region_text = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, div)))

            if region_text.text != "":
                #print("hh ",region_text.get_attribute('innerHTML'))
                region_text1 = region_text.find_element_by_xpath(".//div[@class='clearfix _ikh _1hx']")
                textarea = region_text1.find_element_by_xpath(".//textarea[@class='_3en1 _480e navigationFocus']")
                textarea.send_keys(str(text))

                driver.execute_script("return arguments[0].scrollIntoView(true);", region_text)
                time.sleep(5)

                button_post = region_text.find_element_by_xpath(".//div[@class='_2ph- _4-u3']")
                button_post = button_post.find_element_by_xpath(".//button")

                driver.implicitly_wait(10)
                ActionChains(driver).move_to_element(button_post).click(button_post).perform()
                print("comment succeful!")
            else:
                print("region_text not exist!")
        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        #driver.get('http://icanhazip.com/')
        #ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        #result = {'user': user, 'pass': pw, 'ip': ip, 'profile_url': page_url}
        driver.close()
        self.drivers[driver_index] = None

