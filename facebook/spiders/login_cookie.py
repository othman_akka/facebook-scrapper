import scrapy
from scrapy.utils.project import get_project_settings
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os
import pickle
import time

class Login_cookie(scrapy.Spider):
    name = 'login_cookie_facebook'
    allowed_domains = ['www.facebook.com/']
    start_urls = ['https://www.facebook.com/']
    login_url = 'https://www.facebook.com/login'
    profile_url = 'https://www.facebook.com/imad.maryol.12382'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.settings = get_project_settings()

    def createCookie(self, driver, login_url, user, password):
        driver.get(login_url)
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'email')))
        username_field = driver.find_element_by_xpath('//input[@name="email"]')
        username_field.send_keys(user)
        password_field = driver.find_element_by_xpath('//input[@name="pass"]')
        password_field.send_keys(password)
        driver.find_element_by_xpath('//button[@name="login"]').click()
        current_url = driver.current_url
        WebDriverWait(driver, 20).until(EC.url_changes(current_url))
        cookies_ = driver.get_cookies()
        pickle.dump(cookies_, open('cookie_file.pkl', 'wb'))
        cookies = {}
        for item in cookies_:
            cookies[item['name']] = item['value']
        return cookies

    def loadCookie(self,driver, login_url, user, password):
        # if file of cookie exists
        if os.path.exists('cookie_file.pickle'):
            cookies = pickle.load(open('cookie_file.pkl', 'rb'))
        else:
            cookies = self.createCookie(driver, login_url, user, password)
        return cookies

    def parse(self, response):
        driver = webdriver.Chrome(executable_path=self.settings.get('SELENIUM_DRIVER_EXECUTABLE_PATH'))
        user = "jamal.kandri123@gmail.com"
        password = "rim1995"
        cookies = self.loadCookie(driver, self.login_url, user, password)
        for cookie in cookies:
            driver.add_cookie({
                "domain": ".facebook.com",
                "name": cookie,
                "value": cookies[cookie],
                "path": '/',
                "expires": None
            })
        time.sleep(1)
        driver.refresh()
        driver.get(self.profile_url)
        time.sleep(10)
        driver.close()
