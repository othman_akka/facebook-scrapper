from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time

class Reply_post_page(Base):
    name = 'reply_comment_page'
    redis_key = 'reply_comment_page:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        page_url = response.meta['profile_url']
        post_id = response.meta['post_id']
        comment_id = response.meta['comment_id']
        text = response.meta['text']
        driver = self.drivers[driver_index]
        #driver.maximize_window()

        # go to page
        driver.get(page_url)
        for i in range(1, 10):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(5)
        try:
            posts = "//div[@id='pagelet_timeline_main_column']"
            all_posts = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, posts)))
            all_posts.find_element_by_xpath(".//div[@class='_1xnd']")

            if all_posts.text != "":
                for post_field in all_posts.find_elements_by_xpath(".//div[@class='_4-u2 _4-u8']"):
                    post = post_field.find_element_by_xpath(".//div[@class='_6a _5u5j _6b']/div[1]")
                    if str(post.get_attribute("id")) == post_id:
                        all_comments = post_field.find_element_by_xpath(".//div[@class='_3w53']")
                        get_all_li = all_comments.find_element_by_xpath(".//div[@class='_3tz_ _7794']")

                        if get_all_li.text != "":
                            view_more_comments = get_all_li.find_element_by_xpath(".//div[@class='_4swz _293g']")

                            driver.implicitly_wait(20)
                            ActionChains(driver).move_to_element(view_more_comments).click(view_more_comments).perform()
                            driver.execute_script("return arguments[0].scrollIntoView(true);", all_comments)
                            time.sleep(5)

                        ul = all_comments.find_element_by_xpath(".//ul[@class='_7791']")

                        index = 0
                        for li_field in ul.find_elements_by_xpath("li"):
                            if index == -1:
                                break
                            else:
                                comment_filed1 = li_field.find_element_by_xpath(".//div[1]/div")
                                if str(comment_filed1.get_attribute("data-ft")) == comment_id:
                                    reply = comment_filed1.find_element_by_xpath(".//div[2]")
                                    reply = reply.find_element_by_xpath(".//ul[@class='_6coi _6qw9']")
                                    reply = reply.find_element_by_xpath(".//li[2]")

                                    driver.implicitly_wait(10)
                                    ActionChains(driver).move_to_element(reply).click(reply).perform()

                                    driver.execute_script("return arguments[0].scrollIntoView(true);", li_field)
                                    time.sleep(5)

                                    reply_text = li_field.find_element_by_xpath(".//div[@class='_2h2j']")

                                    reply_text = reply_text.find_element_by_xpath(".//div[@class='_1mf _1mj']")
                                    reply_text.send_keys(str(text))
                                    reply_text.send_keys(Keys.ENTER)
                                    print("reply succeful!")
                                    index = -1
                                    break

                                else:
                                    comment_filed2 = li_field.find_element_by_xpath(".//div[@class='_2h2j']")

                                    if comment_filed2.text != "":
                                        if "reply" in comment_filed2.text.split(" ") or "replies" in comment_filed2.text.split(" "):
                                            span = li_field.find_element_by_xpath(".//span[@class='_4sso _4ssp']")
                                            driver.implicitly_wait(10)
                                            ActionChains(driver).move_to_element(span).click(span).perform()

                                        driver.execute_script("return arguments[0].scrollIntoView(true);", ul)
                                        time.sleep(3)

                                        ul = comment_filed2.find_element_by_xpath(".//ul")
                                        for li_field in ul.find_elements_by_xpath("li"):
                                            comment_filed = li_field.find_element_by_xpath(".//div/div")
                                            if str(comment_filed.get_attribute("data-ft")) == comment_id:
                                                reply = comment_filed.find_element_by_xpath(".//div[2]")
                                                reply = reply.find_element_by_xpath(".//ul[@class='_6coi _6qw9']")
                                                reply = reply.find_element_by_xpath(".//li[2]")

                                                driver.implicitly_wait(10)
                                                ActionChains(driver).move_to_element(reply).click(reply).perform()

                                                driver.execute_script("return arguments[0].scrollIntoView(true);",
                                                                      comment_filed2)
                                                time.sleep(5)

                                                reply_text = comment_filed2.find_element_by_xpath(
                                                    ".//div[@class='_1mf _1mj']")
                                                reply_text.send_keys(str(text))
                                                reply_text.send_keys(Keys.ENTER)
                                                print("reply succeful!")
                                                index = -1
                                                break

                        #bare_comment = bare_comment.find_element_by_xpath(".//div[@class='_1mf _1mj']")
                        #bare_comment.send_keys(str(text))
                        #bare_comment.send_keys(Keys.ENTER)
                        print("comment succeful!")
                        break
            else:
                print("problem in posts!")
        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'page_url': page_url}
        driver.close()
        self.drivers[driver_index] = None

