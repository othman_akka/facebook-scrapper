from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
import time

class Post_image_group(Base):
    name = 'post_image_group'
    redis_key = 'post_image_group:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        group_url = response.meta['profile_url']
        text = response.meta['text']
        image_path = response.meta['image_path']
        driver = self.drivers[driver_index]

        # go to group
        driver.get(group_url)

        try:
            div = "//div[@id='pagelet_group_composer']"
            div = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, div)))
            if div.text != "":
                div_image = div.find_element_by_xpath(".//div[@class='_4zoz  _4cw _4-u3']")
                div_photo = div_image.find_element_by_xpath(".//div[@class='_16ve _7i31']")
                div_photo = div_photo.find_element_by_xpath(".//ul[@class='_16vg _1oxv']/li[1]")
                div_photo = div_photo.find_element_by_xpath(".//div[@class='_3jk']/input")
                div_photo.send_keys(image_path)

                driver.execute_script("return arguments[0].scrollIntoView(true);", div)
                time.sleep(5)

                div_title = div_image.find_element_by_xpath(".//div[@class='_4wbt']")
                texte = div_title.find_element_by_xpath(".//div[@class='_5rpb']")
                texte = texte.find_element_by_xpath(".//div[@class='_1mf _1mj']")
                texte.send_keys(str(text))

                div_button = div_image.find_element_by_xpath(".//div[@class='_1j2v']")
                button = div_button.find_element_by_xpath(".//div[@class='_ohf rfloat']")
                button = button.find_element_by_xpath(".//button")
                driver.implicitly_wait(15)
                ActionChains(driver).move_to_element(button).click(button).perform()

            else:
                printa("div is Null!")

        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'profile_url': group_url}
        driver.close()
        self.drivers[driver_index] = None
        return result

