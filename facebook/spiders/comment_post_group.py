from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time

class Comment_post_group(Base):
    name = 'comment_post_group'
    redis_key = 'comment_post_group:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        group_url = response.meta['profile_url']
        post_id = response.meta['post_id']
        text = response.meta['text']
        driver = self.drivers[driver_index]
        #driver.maximize_window()

        # go to group
        driver.get(group_url)
        for i in range(1, 7):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(5)

        try:
            posts = "//div[@id='pagelet_group_mall']"
            all_posts = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, posts)))
            if all_posts.text != "":
                post = all_posts.find_element_by_xpath(".//div[@id='" + post_id + "']")
                bare_comment = post.find_element_by_xpath(".//div[@class='_4299']")
                bare_comment = bare_comment.find_element_by_xpath(".//div[@class='_3w53']")
                bare_comment1 = bare_comment.find_element_by_xpath(".//form[@class=' _129h']")

                driver.implicitly_wait(10)
                ActionChains(driver).move_to_element(bare_comment1).click(bare_comment1).perform()

                driver.execute_script("return arguments[0].scrollIntoView(true);", bare_comment)
                time.sleep(5)

                bare_comment = bare_comment.find_element_by_xpath(".//div[@class='_1mf _1mj']")
                bare_comment.send_keys(str(text))
                bare_comment.send_keys(Keys.ENTER)
                print("comment succeful!")
            else:
                print("problem in post!")

        except TimeoutException:
            print("Loading took too much time!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'group_url': group_url}
        driver.close()
        self.drivers[driver_index] = None

