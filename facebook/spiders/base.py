import json
import zipfile
from scrapy.utils.project import get_project_settings
from scrapy_redis.spiders import RedisSpider
from scrapy import Request
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Base(RedisSpider):
    name = 'base_facebook'
    redis_key = 'base:facebook'
    login_url = 'https://www.facebook.com/login'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.settings = get_project_settings()
        self.drivers = [None] * 10 # 10 empty browsers instance each time we recieve request from redis we start new instace in empty index

    start_urls = [] # empty start_urls to make spiders in IDLE mode after start and wait for command from redis

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url, callback=self.parse)

    def get_chromedriver(self, use_proxy=False, PROXY_HOST=None, PROXY_PORT=None, PROXY_USER=None, PROXY_PASS=None, user_agent=None):
        manifest_json = """
            {
                "version": "1.0.0",
                "manifest_version": 2,
                "name": "Chrome Proxy",
                "permissions": [
                    "proxy",
                    "tabs",
                    "unlimitedStorage",
                    "storage",
                    "<all_urls>",
                    "webRequest",
                    "webRequestBlocking"
                ],
                "background": {
                    "scripts": ["background.js"]
                },
                "minimum_chrome_version":"22.0.0"
            }
            """

        background_js = """
            var config = {
                    mode: "fixed_servers",
                    rules: {
                    singleProxy: {
                        scheme: "http",
                        host: "%s",
                        port: parseInt(%s)
                    },
                    bypassList: ["localhost"]
                    }
                };

            chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

            function callbackFn(details) {
                return {
                    authCredentials: {
                        username: "%s",
                        password: "%s"
                    }
                };
            }

            chrome.webRequest.onAuthRequired.addListener(
                        callbackFn,
                        {urls: ["<all_urls>"]},
                        ['blocking']
            );
            """ % (PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS)

         # You can disable the browser notifications, using chrome options.
        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)

        if use_proxy:
            pluginfile = 'proxy_auth_plugin.zip'

            with zipfile.ZipFile(pluginfile, 'w') as zp:
                zp.writestr("manifest.json", manifest_json)
                zp.writestr("background.js", background_js)
            chrome_options.add_extension(pluginfile)
        if user_agent:
            chrome_options.add_argument('--user-agent=%s' % user_agent)
        driver = webdriver.Chrome(chrome_options=chrome_options,
                                  executable_path=self.settings.get('SELENIUM_DRIVER_EXECUTABLE_PATH'))
        return driver

    def make_requests_from_url(self, query):
        """
        the main entry from redis if spiders is IDLE mode it will get next command from here
        :param query: the query from redis it's json object
        :return: request instance in this case we work with selenium so we send to scrapy fake request
        """
        data = json.loads(query)
        user = data.get('user', None)
        pw = data.get('pass', None)
        profile_url = data.get('url', None)
        post_id = data.get('post_id',None)
        comment_id = data.get('comment_id',None)
        text = data.get('text', None)
        image_path = data.get('image_path', None)
        PROXY_HOST = data.get('proxy_host', None)
        PROXY_PORT = data.get('proxy_port', None)
        PROXY_USER = data.get('proxy_user', None)
        PROXY_PASS = data.get('proxy_pass', None)

        # TODO: this for loop will return index, driver even if all drivers are busy !!!!! need function to return
        #  Boll and if all drivers busy push back the request again to redis or wait a while and test again !!!!!
        for index, driver in enumerate(self.drivers):
            if driver is None:
                break

        # if PROXY_HOST exist create instance with proxy else normal instance
        if PROXY_HOST:
            driver = self.get_chromedriver(use_proxy=True, PROXY_HOST=PROXY_HOST, PROXY_PORT=PROXY_PORT, PROXY_USER=PROXY_USER, PROXY_PASS=PROXY_PASS)
        else:
            driver = self.get_chromedriver()
        # set driver in the index and do login logic
        self.drivers[index] = driver
        driver.get(self.login_url)
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'email')))
        username_field = driver.find_element_by_xpath('//input[@name="email"]')
        username_field.send_keys(user)
        password_field = driver.find_element_by_xpath('//input[@name="pass"]')
        password_field.send_keys(pw)
        driver.find_element_by_xpath('//button[@name="login"]').click()
        current_url = driver.current_url
        WebDriverWait(driver, 20).until(EC.url_changes(current_url))

        # return fake Request to get to next method and pass the vars in meta
        return Request('http://icanhazip.com/',
                       callback=self.parse,
                       meta={'driver_index': index,
                             'user': user,
                             'pw': pw,
                             'profile_url': profile_url,
                             'post_id' : post_id,
                             'comment_id': comment_id,
                             'text' :text,
                             'image_path':image_path
                             },
                       dont_filter=True)

