from facebook.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import time
import os
import urllib.request
import shutil

class Info_login(Base):
    name = 'info_person_facebook'
    redis_key = 'info_person:facebook'

    def parse(self, response):
        # get vars from request
        user = response.meta['user']
        driver_index = response.meta['driver_index']
        pw = response.meta['pw']
        profile_url = response.meta['profile_url']
        text = response.meta['text']
        driver = self.drivers[driver_index]

        # go to profile
        driver.get(profile_url)
        followers = []

        for i in range(1,3):
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(3)

        try:
            div_name = "//div[@id='timeline_top_section']"
            div_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, div_name)))

            div_name = div_name.find_element_by_xpath(".//div[@class='cover']")
            div_name = div_name.find_element_by_xpath(".//div[@class='_2nlj _2xc6']")
            name_profile = div_name.find_element_by_xpath(".//h1/span/a").text

            #os.rmdir("Profiles//"+name_profile)
            shutil.rmtree("Profiles//"+name_profile , ignore_errors=True)
            os.mkdir("Profiles//"+name_profile)

            timeline = "//div[@id='timeline_tab_content']"
            timeline = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, timeline)))

            info = timeline.find_element_by_xpath(".//div[@id='timeline_small_column']")
            driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(3)

            div_intro = info.find_element_by_xpath(".//div[2]")
            div_photo = info.find_element_by_xpath(".//div[@class='_5h60']")
            div_friends = info.find_element_by_xpath(".//div[4]")
            div_events = info.find_element_by_xpath(".//div[5]")

            if div_intro.text !="":
                div_intro = div_intro.find_element_by_xpath(".//div[@id='intro_container_id']")
                ul = div_intro.find_element_by_xpath(".//ul[@class='uiList _3-8x _2pic _4kg']")
                file = open("Profiles//"+name_profile+"//info.txt", "w+")
                for li in ul.find_elements_by_xpath(".//li[@class='_1zw6 _md0 _5h-n _5vb9']"):
                    info = li.find_element_by_xpath(".//div[@class='_42ef']")
                    file.write(str(info.text)+"\n")
                file.close()

                # sometimes exist pictures in part of intro
                div_pictures = div_intro.find_element_by_xpath(".//div[@class='_3c-4 _5nxu _3-8m']")
                cpt = 0
                for link in div_pictures.find_elements_by_xpath(".//div/a"):
                    url_image = link.find_element_by_xpath(".//div/div/div/img")
                    urllib.request.urlretrieve(url_image.get_attribute('src'),"Profiles//" + name_profile + "//imageIntro" + str(cpt) + ".png")
                    cpt += 1

            else:
                print("not found info!")

            if div_photo.text !="":
                div_photo = div_photo.find_element_by_xpath(".//div[@class='_4-u2 _4-u8']")
                table = div_photo.find_element_by_xpath(".//table[@class='uiGrid _51mz _1m6c']")
                cpt=0
                for tr in table.find_elements_by_xpath(".//tr[@class='_51mx']"):
                    for td in tr.find_elements_by_xpath(".//td[@class='_51m-']"):
                        url_image = td.find_element_by_xpath(".//a/div/img")
                        urllib.request.urlretrieve(url_image.get_attribute('src'), "Profiles//"+name_profile+"//image"+str(cpt)+".png")
                        cpt += 1

            else:
                print("not found pictures!")

            if div_events.text !="":
                div_events = div_events.find_element_by_xpath(".//div[@class='_4-u2 _4-u8']")
                table = div_events.find_element_by_xpath(".//table[@class='uiGrid _51mz']")
                file = open("Profiles//" + name_profile + "//events.txt", "w+")
                for tr in table.find_elements_by_xpath(".//tr[@class='_51mx']"):
                    for td in tr.find_elements_by_xpath(".//td[@class='_51m-']"):
                        event = td.find_element_by_xpath(".//a/div")
                        file.write(str(event.text) + "\n\n")
            else:
                print("Not found events!")

            driver.save_screenshot("image.png")
        except TimeoutException:
            print("this Profile without followers!")

        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'user': user, 'pass': pw, 'ip': ip, 'profile_url': profile_url}
        driver.close()
        self.drivers[driver_index] = None

